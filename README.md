# Aston Insurance Demo
1. Run `mvn clean install`
2. Run `cd target`
3. Run `java -jar insurance-demo-0.0.1-SNAPSHOT.jar`
4. Run curl request or use Postman/Insomnia
```
curl --request POST \
  --url http://localhost:8080/rest/main \
  --header 'Content-Type: application/json' \
  --data '{
  "type": "VARIABLE",
  "bundle": "EXTENDED",
  "bonuses": [
    "SPORT"
  ],
  "numberOfPersons": 2,
  "start": "2021-06-13",
  "end": "2021-06-16"
}'
```
5. check data in database http://localhost:8080/h2-console/