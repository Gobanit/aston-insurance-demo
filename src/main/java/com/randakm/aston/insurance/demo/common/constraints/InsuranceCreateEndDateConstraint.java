package com.randakm.aston.insurance.demo.common.constraints;

import com.randakm.aston.insurance.demo.common.dto.InsuranceCreateDto;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Validation constraint for End date on {@link InsuranceCreateDto} class.
 *
 */
@Documented
@Constraint(validatedBy = InsuranceCreateEndDateConstraintValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface InsuranceCreateEndDateConstraint {
  String message() default "Invalid insurance end date";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}