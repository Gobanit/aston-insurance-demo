package com.randakm.aston.insurance.demo;

import static com.randakm.aston.insurance.demo.common.InsuranceBundle.BASIC;
import static com.randakm.aston.insurance.demo.common.InsuranceBundle.EXTENDED;
import static com.randakm.aston.insurance.demo.common.InsuranceBundle.EXTRA;
import static com.randakm.aston.insurance.demo.common.InsuranceType.VARIABLE;
import static com.randakm.aston.insurance.demo.common.InsuranceType.YEAR;

import com.randakm.aston.insurance.demo.common.InsuranceBonus;
import com.randakm.aston.insurance.demo.common.InsuranceBundle;
import com.randakm.aston.insurance.demo.common.InsuranceType;
import com.randakm.aston.insurance.demo.persistence.data.BonusPriceRate;
import com.randakm.aston.insurance.demo.persistence.data.BundlePriceRate;
import com.randakm.aston.insurance.demo.persistence.repo.BonusPriceRateRepository;
import com.randakm.aston.insurance.demo.persistence.repo.BundlePriceRateRepository;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

/**
 * Implementation for {@link DataInitializer} interface.
 *
 */
@Named
public class DataInitializerImpl implements DataInitializer {

  private final BundlePriceRateRepository bundlePriceRepo;
  private final BonusPriceRateRepository bonusPriceRepo;

  /**
   * @param bundlePriceRepo
   * @param bonusPriceRepo
   */
  @Inject
  public DataInitializerImpl(BundlePriceRateRepository bundlePriceRepo, BonusPriceRateRepository bonusPriceRepo) {
    super();
    this.bundlePriceRepo = bundlePriceRepo;
    this.bonusPriceRepo = bonusPriceRepo;
  }

  @Override
  @Transactional
  public void initData() {
    add(BASIC, VARIABLE, 1.2);
    add(BASIC, YEAR, 39);
    add(EXTENDED, VARIABLE, 1.8);
    add(EXTENDED, YEAR, 49);
    add(EXTRA, VARIABLE, 2.4);
    add(EXTRA, YEAR, 59);

    add(InsuranceBonus.CANCELLATION, VARIABLE, 0.5);
    add(InsuranceBonus.CANCELLATION, YEAR, 0.2);
    add(InsuranceBonus.SPORT, VARIABLE, 0.3);
    add(InsuranceBonus.SPORT, YEAR, 0.1);

  }

  private void add(InsuranceBundle bundle, InsuranceType type, double price) {
    var item = new BundlePriceRate();
    item.setBundle(bundle);
    item.setType(type);
    item.setPrice(new BigDecimal(price));
    bundlePriceRepo.save(item);
  }

  private void add(InsuranceBonus bonus, InsuranceType type, double price) {
    var item = new BonusPriceRate();
    item.setBonus(bonus);
    item.setType(type);
    item.setPriceModifier(new BigDecimal(price));
    bonusPriceRepo.save(item);
  }
}
