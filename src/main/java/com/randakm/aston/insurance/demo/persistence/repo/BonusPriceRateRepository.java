package com.randakm.aston.insurance.demo.persistence.repo;

import com.randakm.aston.insurance.demo.common.InsuranceBonus;
import com.randakm.aston.insurance.demo.common.InsuranceType;
import com.randakm.aston.insurance.demo.persistence.data.BonusPriceRate;

import org.springframework.data.repository.CrudRepository;

/**
 * Repository for CRUD operation over {@link BonusPriceRate} entity.
 *
 */
public interface BonusPriceRateRepository extends CrudRepository<BonusPriceRate, String> {

  /**
   * Finds all price rates matching specified insurance bonus and type.
   * 
   * @param bonus
   * @param type
   * @return found entity or null if not matched
   */
  BonusPriceRate findByBonusAndType(InsuranceBonus bonus, InsuranceType type);
}
