package com.randakm.aston.insurance.demo.persistence.repo;

import com.randakm.aston.insurance.demo.persistence.data.Insurance;

import org.springframework.data.repository.CrudRepository;

/**
 * Repository for CRUD operation over {@link Insurance} entity.
 *
 */
public interface InsuranceRepository extends CrudRepository<Insurance, String> {
}
