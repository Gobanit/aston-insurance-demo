package com.randakm.aston.insurance.demo.service.facade;

import com.randakm.aston.insurance.demo.common.dto.InsuranceCreateDto;
import com.randakm.aston.insurance.demo.common.dto.InsuranceDto;

import java.util.List;

import javax.validation.Valid;

/**
 * Facade for handling Insurance use cases. Methods should be atomic and input
 * their validated.
 *
 */
public interface InsuranceFacade {

  /**
   * Register new insurance based on provided input data.
   * 
   * @param dto - insurance to create
   * @return created insurance with calculated price
   */
  InsuranceDto registerInsurance(@Valid InsuranceCreateDto dto);

  /**
   * Finds all available insurances in stored by application.
   * 
   * @return list of insurances
   */
  List<InsuranceDto> getAll();
}
