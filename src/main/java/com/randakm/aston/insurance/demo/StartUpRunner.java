package com.randakm.aston.insurance.demo;

import java.util.Arrays;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;

/**
 * Class for executing some logic after startup
 *
 */
@Named
public class StartUpRunner implements ApplicationRunner {
  private static final Logger LOGGER = LoggerFactory.getLogger(StartUpRunner.class);

  @Inject
  private ApplicationContext appContext;

  @Inject
  private DataInitializer dataInitializer;
  
  @Value("${app.initSampleData}" )
  private boolean initSampleData;

  /**
   * Method called by Spring after context is initialized.
   */
  @Override
  public void run(ApplicationArguments args) throws Exception {
    testLogging();
    inspectBeans(appContext);
    startup();
  }

  private void testLogging() {
    LOGGER.info("Logging info");
    LOGGER.debug("Logging debug");
    LOGGER.trace("Logging trace");
  }

  private void inspectBeans(ApplicationContext appContext) {
    if (!LOGGER.isDebugEnabled())
      return;

    LOGGER.debug("Beans registered in Spring context:");
    String[] beanNames = appContext.getBeanDefinitionNames();
    Arrays.sort(beanNames);
    for (String beanName : beanNames) {
      LOGGER.debug("Bean: " + beanName);
    }
    LOGGER.debug("Inspection done!");
  }

  private void startup() {
    if(initSampleData) {
      dataInitializer.initData();
      LOGGER.info("Sample data initialized!");
    } else {
      LOGGER.info("Sample data init skipped!");
    }
  }

}
