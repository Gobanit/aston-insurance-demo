package com.randakm.aston.insurance.demo.web;

import com.randakm.aston.insurance.demo.common.InsuranceBonus;
import com.randakm.aston.insurance.demo.common.InsuranceBundle;
import com.randakm.aston.insurance.demo.common.InsuranceType;
import com.randakm.aston.insurance.demo.common.dto.InsuranceCreateDto;
import com.randakm.aston.insurance.demo.common.dto.InsuranceDto;
import com.randakm.aston.insurance.demo.service.facade.InsuranceFacade;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Main controller for handling basic requests.
 *
 */
@RestController
@RequestMapping("rest/main")
public class MainController {

  private final InsuranceFacade insuranceFacade;

  @Inject
  public MainController(InsuranceFacade insuranceFacade) {
    super();
    this.insuranceFacade = insuranceFacade;
  }

  /**
   * Returns some example {@link InsuranceCreateDto}
   */
  @GetMapping(value = "default", produces = "application/json")
  public InsuranceCreateDto priceCalculate() {
    var ins = new InsuranceCreateDto();
    ins.setType(InsuranceType.VARIABLE);
    ins.setBundle(InsuranceBundle.EXTENDED);
    ins.setBonuses(Set.of(InsuranceBonus.SPORT));
    ins.setNumberOfPersons(2);
    ins.setStart(LocalDate.now());
    ins.setEnd(ins.getStart().plus(3, ChronoUnit.DAYS));

    return ins;
  }

  @PostMapping(value = "", consumes = "application/json", produces = "application/json")
  public InsuranceDto registerInsurance(@RequestBody InsuranceCreateDto ins) {
    return insuranceFacade.registerInsurance(ins);
  }

  @GetMapping(value = "", produces = "application/json")
  public List<InsuranceDto> getAllInsurances() {
    return insuranceFacade.getAll();
  }
}
