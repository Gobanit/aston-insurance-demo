package com.randakm.aston.insurance.demo.common.dto;

import com.randakm.aston.insurance.demo.common.InsuranceBonus;
import com.randakm.aston.insurance.demo.common.InsuranceBundle;
import com.randakm.aston.insurance.demo.common.InsuranceType;
import com.randakm.aston.insurance.demo.common.constraints.InsuranceCreateEndDateConstraint;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Data transfer object for creating new Insurance.
 *
 */
@InsuranceCreateEndDateConstraint
public class InsuranceCreateDto {
  @NotNull
  private InsuranceType type;
  @NotNull
  private InsuranceBundle bundle;
  private Set<InsuranceBonus> bonuses = new HashSet<>();
  @NotNull
  @Min(1)
  @Max(3)
  private Integer numberOfPersons;
  @NotNull
  private LocalDate start;
  private LocalDate end;

  /**
   * Getter for type.
   *
   * @return the type
   */
  public InsuranceType getType() {
    return type;
  }

  /**
   * Setter for type.
   *
   * @param type - the type to set
   */
  public void setType(InsuranceType type) {
    this.type = type;
  }

  /**
   * Getter for bundle.
   *
   * @return the bundle
   */
  public InsuranceBundle getBundle() {
    return bundle;
  }

  /**
   * Setter for bundle.
   *
   * @param bundle - the bundle to set
   */
  public void setBundle(InsuranceBundle bundle) {
    this.bundle = bundle;
  }

  /**
   * Getter for bonuses.
   *
   * @return the bonuses
   */
  public Set<InsuranceBonus> getBonuses() {
    return bonuses;
  }

  /**
   * Setter for bonuses.
   *
   * @param bonuses - the bonuses to set
   */
  public void setBonuses(Set<InsuranceBonus> bonuses) {
    this.bonuses = bonuses;
  }

  /**
   * Getter for numberOfPersons.
   *
   * @return the numberOfPersons
   */
  public Integer getNumberOfPersons() {
    return numberOfPersons;
  }

  /**
   * Setter for numberOfPersons.
   *
   * @param numberOfPersons - the numberOfPersons to set
   */
  public void setNumberOfPersons(Integer numberOfPersons) {
    this.numberOfPersons = numberOfPersons;
  }

  /**
   * Getter for start.
   *
   * @return the start
   */
  public LocalDate getStart() {
    return start;
  }

  /**
   * Setter for start.
   *
   * @param start - the start to set
   */
  public void setStart(LocalDate start) {
    this.start = start;
  }

  /**
   * Getter for end.
   *
   * @return the end
   */
  public LocalDate getEnd() {
    return end;
  }

  /**
   * Setter for end.
   *
   * @param end - the end to set
   */
  public void setEnd(LocalDate end) {
    this.end = end;
  }

}
