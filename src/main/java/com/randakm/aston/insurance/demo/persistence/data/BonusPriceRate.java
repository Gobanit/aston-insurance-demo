package com.randakm.aston.insurance.demo.persistence.data;

import com.randakm.aston.insurance.demo.common.InsuranceBonus;
import com.randakm.aston.insurance.demo.common.InsuranceType;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entity for storing Bonus price rate.
 *
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "bonus", "type" }))
public class BonusPriceRate {

  @Id
  @GeneratedValue
  private Long id;
  @Enumerated(EnumType.STRING)
  private InsuranceBonus bonus;
  @Enumerated(EnumType.STRING)
  private InsuranceType type;
  private BigDecimal priceModifier;

  /**
   * Getter for id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Setter for id.
   *
   * @param id - the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Getter for bonus.
   *
   * @return the bonus
   */
  public InsuranceBonus getBonus() {
    return bonus;
  }

  /**
   * Setter for bonus.
   *
   * @param bonus - the bonus to set
   */
  public void setBonus(InsuranceBonus bonus) {
    this.bonus = bonus;
  }

  /**
   * Getter for type.
   *
   * @return the type
   */
  public InsuranceType getType() {
    return type;
  }

  /**
   * Setter for type.
   *
   * @param type - the type to set
   */
  public void setType(InsuranceType type) {
    this.type = type;
  }

  /**
   * Getter for priceModifier.
   *
   * @return the priceModifier
   */
  public BigDecimal getPriceModifier() {
    return priceModifier;
  }

  /**
   * Setter for priceModifier.
   *
   * @param priceModifier - the priceModifier to set
   */
  public void setPriceModifier(BigDecimal priceModifier) {
    this.priceModifier = priceModifier;
  }

}
