package com.randakm.aston.insurance.demo.common;

/**
 * Enumeration of supported bonuses to insurance.
 *
 */
public enum InsuranceBonus {
  SPORT, CANCELLATION
}
