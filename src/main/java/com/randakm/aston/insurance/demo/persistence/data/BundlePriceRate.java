package com.randakm.aston.insurance.demo.persistence.data;

import com.randakm.aston.insurance.demo.common.InsuranceBundle;
import com.randakm.aston.insurance.demo.common.InsuranceType;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entity for storing Bundle price rate.
 *
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "bundle", "type" }))
public class BundlePriceRate {

  @Id
  @GeneratedValue
  private Long id;
  @Enumerated(EnumType.STRING)
  private InsuranceBundle bundle;
  @Enumerated(EnumType.STRING)
  private InsuranceType type;
  private BigDecimal price;

  /**
   * Getter for id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Setter for id.
   *
   * @param id - the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Getter for bundle.
   *
   * @return the bundle
   */
  public InsuranceBundle getBundle() {
    return bundle;
  }

  /**
   * Setter for bundle.
   *
   * @param bundle - the bundle to set
   */
  public void setBundle(InsuranceBundle bundle) {
    this.bundle = bundle;
  }

  /**
   * Getter for type.
   *
   * @return the type
   */
  public InsuranceType getType() {
    return type;
  }

  /**
   * Setter for type.
   *
   * @param type - the type to set
   */
  public void setType(InsuranceType type) {
    this.type = type;
  }

  /**
   * Getter for price.
   *
   * @return the price
   */
  public BigDecimal getPrice() {
    return price;
  }

  /**
   * Setter for price.
   *
   * @param price - the price to set
   */
  public void setPrice(BigDecimal price) {
    this.price = price;
  }

}
