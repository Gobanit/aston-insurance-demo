package com.randakm.aston.insurance.demo.service.facade;

import com.randakm.aston.insurance.demo.common.dto.InsuranceCreateDto;
import com.randakm.aston.insurance.demo.common.dto.InsuranceDto;
import com.randakm.aston.insurance.demo.persistence.data.Insurance;
import com.randakm.aston.insurance.demo.persistence.repo.InsuranceRepository;
import com.randakm.aston.insurance.demo.service.PriceCalculationService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.validation.annotation.Validated;

/**
 * Implementation of {@link InsuranceFacade}.
 *
 */
@Named
@Validated
@Transactional
public class InsuranceFacadeImpl implements InsuranceFacade {

  private final PriceCalculationService priceService;
  private final InsuranceRepository insuranceRepo;
  private final ModelMapper mapper;

  /**
   * @param priceService
   * @param insuranceRepo
   */
  @Inject
  public InsuranceFacadeImpl(PriceCalculationService priceService, InsuranceRepository insuranceRepo) {
    super();
    this.priceService = priceService;
    this.insuranceRepo = insuranceRepo;
    this.mapper = new ModelMapper();
  }

  @Override
  public InsuranceDto registerInsurance(InsuranceCreateDto dto) {
    var ins = mapper.map(dto, Insurance.class); // used so that we dont copy fields manually
    var price = priceService.calculatePrice(ins);
    ins.setPrice(price);
    ins = insuranceRepo.save(ins);
    return mapper.map(ins, InsuranceDto.class);
  }

  @Override
  public List<InsuranceDto> getAll() {
    var stream = StreamSupport.stream(insuranceRepo.findAll().spliterator(), true);
    var dtos = stream.map((e) -> mapper.map(e, InsuranceDto.class)).collect(Collectors.toList());
    return dtos;
  }

}
