package com.randakm.aston.insurance.demo.service;

import com.randakm.aston.insurance.demo.common.InsuranceBundle;
import com.randakm.aston.insurance.demo.common.InsuranceBonus;
import com.randakm.aston.insurance.demo.common.InsuranceType;
import com.randakm.aston.insurance.demo.persistence.repo.BonusPriceRateRepository;
import com.randakm.aston.insurance.demo.persistence.repo.BundlePriceRateRepository;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Implementation of {@link PriceRateService}.
 *
 */
@Named
public class PriceRateServiceImpl implements PriceRateService {

  private final BundlePriceRateRepository bundlePriceRepo;
  private final BonusPriceRateRepository bonusPriceRepo;

  /**
   * @param bundlePriceRepo
   * @param bonusPriceRepo
   */
  @Inject
  public PriceRateServiceImpl(BundlePriceRateRepository bundlePriceRepo, BonusPriceRateRepository bonusPriceRepo) {
    super();
    this.bundlePriceRepo = bundlePriceRepo;
    this.bonusPriceRepo = bonusPriceRepo;
  }

  @Override
  public BigDecimal bonusPriceModifier(InsuranceBonus bonus, InsuranceType type) {
    var priceRate = bonusPriceRepo.findByBonusAndType(bonus, type);
    if (priceRate == null)
      throw new IllegalStateException(
          String.format("No BonusPriceRate available for bonus %s and type %s", bonus, type));

    return priceRate.getPriceModifier();
  }

  @Override
  public BigDecimal bundlePrice(InsuranceBundle bundle, InsuranceType type) {
    var priceRate = bundlePriceRepo.findByBundleAndType(bundle, type);
    if (priceRate == null)
      throw new IllegalStateException(
          String.format("No BundlePriceRate available for bundle %s and type %s", bundle, type));

    return priceRate.getPrice();
  }

}
