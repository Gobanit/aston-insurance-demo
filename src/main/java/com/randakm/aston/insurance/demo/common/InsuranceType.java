package com.randakm.aston.insurance.demo.common;

/**
 * Type of insurance contract.
 *
 */
public enum InsuranceType {
  YEAR, VARIABLE
}
