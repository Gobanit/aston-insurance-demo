package com.randakm.aston.insurance.demo.persistence.data;

import com.randakm.aston.insurance.demo.common.InsuranceBonus;
import com.randakm.aston.insurance.demo.common.InsuranceBundle;
import com.randakm.aston.insurance.demo.common.InsuranceType;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * Entity for storing Insurance. Some fields values are generated automatically
 * by ORM.
 *
 */
@Entity
public class Insurance {

  @Id
  @GeneratedValue
  private Long id;
  @Enumerated(EnumType.STRING)
  private InsuranceType type;
  @Enumerated(EnumType.STRING)
  private InsuranceBundle bundle;
  @Column
  @Enumerated(EnumType.STRING)
  @ElementCollection(targetClass = InsuranceBonus.class)
  private Set<InsuranceBonus> bonuses = new HashSet<>();
  private Integer numberOfPersons;
  private BigDecimal price;
  private LocalDate start;
  private LocalDate end;
  private Instant created;
  private Instant modified;

  @PrePersist
  private void prePersist() {
    created = Instant.now();
    modified = created;
  }

  @PreUpdate
  private void preUpdate() {
    modified = Instant.now();
  }

  /**
   * Getter for id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Setter for id.
   *
   * @param id - the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Getter for type.
   *
   * @return the type
   */
  public InsuranceType getType() {
    return type;
  }

  /**
   * Setter for type.
   *
   * @param type - the type to set
   */
  public void setType(InsuranceType type) {
    this.type = type;
  }

  /**
   * Getter for bundle.
   *
   * @return the bundle
   */
  public InsuranceBundle getBundle() {
    return bundle;
  }

  /**
   * Setter for bundle.
   *
   * @param bundle - the bundle to set
   */
  public void setBundle(InsuranceBundle bundle) {
    this.bundle = bundle;
  }

  /**
   * Getter for bonuses.
   *
   * @return the bonuses
   */
  public Set<InsuranceBonus> getBonuses() {
    return bonuses;
  }

  /**
   * Setter for bonuses.
   *
   * @param bonuses - the bonuses to set
   */
  public void setBonuses(Set<InsuranceBonus> bonuses) {
    this.bonuses = bonuses;
  }

  /**
   * Getter for numberOfPersons.
   *
   * @return the numberOfPersons
   */
  public Integer getNumberOfPersons() {
    return numberOfPersons;
  }

  /**
   * Setter for numberOfPersons.
   *
   * @param numberOfPersons - the numberOfPersons to set
   */
  public void setNumberOfPersons(Integer numberOfPersons) {
    this.numberOfPersons = numberOfPersons;
  }

  /**
   * Getter for price.
   *
   * @return the price
   */
  public BigDecimal getPrice() {
    return price;
  }

  /**
   * Setter for price.
   *
   * @param price - the price to set
   */
  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  /**
   * Getter for start.
   *
   * @return the start
   */
  public LocalDate getStart() {
    return start;
  }

  /**
   * Setter for start.
   *
   * @param start - the start to set
   */
  public void setStart(LocalDate start) {
    this.start = start;
  }

  /**
   * Getter for end.
   *
   * @return the end
   */
  public LocalDate getEnd() {
    return end;
  }

  /**
   * Setter for end.
   *
   * @param end - the end to set
   */
  public void setEnd(LocalDate end) {
    this.end = end;
  }

  /**
   * Getter for created.
   *
   * @return the created
   */
  public Instant getCreated() {
    return created;
  }

  /**
   * Getter for modified.
   *
   * @return the modified
   */
  public Instant getModified() {
    return modified;
  }

  /**
   * Setter for created.
   *
   * @param created - the created to set
   */
  public void setCreated(Instant created) {
    this.created = created;
  }

  /**
   * Setter for modified.
   *
   * @param modified - the modified to set
   */
  public void setModified(Instant modified) {
    this.modified = modified;
  }

}
