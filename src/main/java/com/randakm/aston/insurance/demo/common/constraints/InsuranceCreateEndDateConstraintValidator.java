package com.randakm.aston.insurance.demo.common.constraints;

import com.randakm.aston.insurance.demo.common.InsuranceType;
import com.randakm.aston.insurance.demo.common.dto.InsuranceCreateDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator for {@link InsuranceCreateEndDateConstraint} constraint.
 *
 */
public class InsuranceCreateEndDateConstraintValidator
    implements ConstraintValidator<InsuranceCreateEndDateConstraint, InsuranceCreateDto> {

  @Override
  public boolean isValid(InsuranceCreateDto value, ConstraintValidatorContext context) {
    // TODO different message should be provided for different outcomes
    
    if (value.getType() == InsuranceType.VARIABLE && value.getEnd() == null) {
      return false;
    }
    if (value.getType() == InsuranceType.YEAR && value.getEnd() != null) {
      return false;
    }

    if (value.getStart() != null && value.getEnd() != null && value.getEnd().isBefore(value.getStart())) {
      return false;
    }

    return true;
  }

}