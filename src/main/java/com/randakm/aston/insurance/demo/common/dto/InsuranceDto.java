package com.randakm.aston.insurance.demo.common.dto;

import com.randakm.aston.insurance.demo.persistence.data.Insurance;

/**
 * Data transfer object for Insurance. Extending the Insurance just to avoid
 * duplicated code (might be neccessary in the future).
 *
 */
public class InsuranceDto extends Insurance {

}
