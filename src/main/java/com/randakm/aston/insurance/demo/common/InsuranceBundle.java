package com.randakm.aston.insurance.demo.common;

/**
 * Enumeration of supported insurance "bundles".
 *
 */
public enum InsuranceBundle {
  BASIC, EXTENDED, EXTRA 
}
