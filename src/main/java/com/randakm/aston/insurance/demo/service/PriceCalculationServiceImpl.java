package com.randakm.aston.insurance.demo.service;

import com.randakm.aston.insurance.demo.persistence.data.Insurance;

import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Implementation of {@link PriceCalculationService}.
 *
 */
@Named
public class PriceCalculationServiceImpl implements PriceCalculationService {

  private final PriceRateService priceRateService;

  /**
   * @param priceRateService
   */
  @Inject
  public PriceCalculationServiceImpl(PriceRateService priceRateService) {
    super();
    this.priceRateService = priceRateService;
  }

  @Override
  public BigDecimal calculatePrice(Insurance insurance) {
    // basic price for bundle of type
    var basePrice = calculateBasePrice(insurance);
    var totalPrice = basePrice;

    // apply modifiers for bonuses
    for (var bonus : insurance.getBonuses()) {
      var bonusPriceModifier = priceRateService.bonusPriceModifier(bonus, insurance.getType());
      var bonusPrice = basePrice.multiply(bonusPriceModifier);
      totalPrice = totalPrice.add(bonusPrice);
    }

    // multiply by persons
    totalPrice = totalPrice.multiply(new BigDecimal(insurance.getNumberOfPersons()));

    // return result
    return totalPrice;
  }

  private BigDecimal calculateBasePrice(Insurance insurance) {
    switch (insurance.getType()) {
      case VARIABLE:
        return baseForVariableType(insurance);
      case YEAR:
        return baseForYearType(insurance);
      default:
        throw new IllegalStateException("Unknown type " + insurance.getType());
    }
  }

  private BigDecimal baseForYearType(Insurance insurance) {
    return priceRateService.bundlePrice(insurance.getBundle(), insurance.getType());
  }

  private BigDecimal baseForVariableType(Insurance insurance) {
    var pricePerDay = priceRateService.bundlePrice(insurance.getBundle(), insurance.getType());
    var daysLength = daysLength(insurance);
    return pricePerDay.multiply(new BigDecimal(daysLength));
  }

  private long daysLength(Insurance insurance) {
    var days = ChronoUnit.DAYS.between(insurance.getStart(), insurance.getEnd());
    return days;
  }

}
