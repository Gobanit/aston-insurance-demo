package com.randakm.aston.insurance.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot initializing class
 *
 */
@SpringBootApplication
public class InsuranceDemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(InsuranceDemoApplication.class, args);
  }

}
