package com.randakm.aston.insurance.demo.service;

import com.randakm.aston.insurance.demo.common.InsuranceBonus;
import com.randakm.aston.insurance.demo.common.InsuranceBundle;
import com.randakm.aston.insurance.demo.common.InsuranceType;

import java.math.BigDecimal;

/**
 * Service for getting products prices.
 *
 */
public interface PriceRateService {

  /**
   * Returns price modifier for specified bonus and type.
   * 
   * @param bonus
   * @param type
   * @return price modifier
   */
  BigDecimal bonusPriceModifier(InsuranceBonus bonus, InsuranceType type);

  /**
   * Returns price for specified bundle and type.
   * 
   * @param bonus
   * @param type
   * @return price modifier
   */
  BigDecimal bundlePrice(InsuranceBundle bundle, InsuranceType type);

}
