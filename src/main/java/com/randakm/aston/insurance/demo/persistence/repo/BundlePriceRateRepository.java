package com.randakm.aston.insurance.demo.persistence.repo;

import com.randakm.aston.insurance.demo.common.InsuranceBundle;
import com.randakm.aston.insurance.demo.common.InsuranceType;
import com.randakm.aston.insurance.demo.persistence.data.BundlePriceRate;

import org.springframework.data.repository.CrudRepository;

/**
 * Repository for CRUD operation over {@link BundlePriceRate} entity.
 *
 */
public interface BundlePriceRateRepository extends CrudRepository<BundlePriceRate, String> {

  /**
   * Finds all price rates matching specified insurance bundle and type.
   * 
   * @param bundle
   * @param type
   * @return found entity or null if not matched
   */
  BundlePriceRate findByBundleAndType(InsuranceBundle bundle, InsuranceType type);
}
