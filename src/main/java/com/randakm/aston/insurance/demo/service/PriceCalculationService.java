package com.randakm.aston.insurance.demo.service;

import com.randakm.aston.insurance.demo.persistence.data.Insurance;

import java.math.BigDecimal;

/**
 * Class for calculating price for insurance.
 *
 */
public interface PriceCalculationService {

  /**
   * Calculates price of the insurance specified.
   * 
   * @return insurance price
   */
  BigDecimal calculatePrice(Insurance insurance);
}
