package com.randakm.aston.insurance.demo;

/**
 * Interface for populating database with default data.
 *
 */
public interface DataInitializer {

  /**
   * Creates basic data in DB.
   */
  void initData();

}