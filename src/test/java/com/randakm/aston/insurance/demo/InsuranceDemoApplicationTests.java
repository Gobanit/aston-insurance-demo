package com.randakm.aston.insurance.demo;

import com.randakm.aston.insurance.demo.common.InsuranceBonus;
import com.randakm.aston.insurance.demo.common.InsuranceBundle;
import com.randakm.aston.insurance.demo.common.InsuranceType;
import com.randakm.aston.insurance.demo.common.dto.InsuranceCreateDto;
import com.randakm.aston.insurance.demo.persistence.repo.BonusPriceRateRepository;
import com.randakm.aston.insurance.demo.persistence.repo.BundlePriceRateRepository;
import com.randakm.aston.insurance.demo.service.facade.InsuranceFacade;
import com.randakm.aston.insurance.demo.web.MainController;

import java.time.LocalDate;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class InsuranceDemoApplicationTests {

  @Inject
  MainController mainController;

  @Inject
  BundlePriceRateRepository bundlePriceRepo;

  @Inject
  BonusPriceRateRepository bonusPriceRepo;

  @Inject
  InsuranceFacade insFacade;

  @Test
  void testIfControllersPresent() {
    Assertions.assertNotNull(mainController);
  }

  @Test
  void testIfDataInitialized() {
    for (var type : InsuranceType.values()) {
      for (var bundle : InsuranceBundle.values()) {
        Assertions.assertNotNull(bundlePriceRepo.findByBundleAndType(bundle, type));
      }
      for (var bonus : InsuranceBonus.values()) {
        Assertions.assertNotNull(bonusPriceRepo.findByBonusAndType(bonus, type));
      }
    }
  }

  @Test
  void testSpringValidationWorks() {
    var ins = new InsuranceCreateDto();
    Assertions.assertThrows(ConstraintViolationException.class, () -> insFacade.registerInsurance(ins));
  }
  
  @Test
  void testValidInsuranceCreation() {
    var ins = new InsuranceCreateDto();
    ins.setType(InsuranceType.YEAR);
    ins.setBundle(InsuranceBundle.EXTRA);
    ins.setStart(LocalDate.now());
    ins.setNumberOfPersons(1);

    var cnt = insFacade.getAll().size();
    insFacade.registerInsurance(ins);
    Assertions.assertEquals(cnt+1, insFacade.getAll().size());
  }
}
