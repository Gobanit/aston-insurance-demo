package com.randakm.aston.insurance.demo.service;

import com.randakm.aston.insurance.demo.common.InsuranceBundle;
import com.randakm.aston.insurance.demo.common.InsuranceBonus;
import com.randakm.aston.insurance.demo.common.InsuranceType;
import com.randakm.aston.insurance.demo.persistence.data.Insurance;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class PriceCalculatorServiceTest {

  private PriceRateService priceRateServ;
  private PriceCalculationService priceCalcServ;

  @BeforeEach
  public void setup() {
    priceRateServ = Mockito.mock(PriceRateService.class);
    priceCalcServ = new PriceCalculationServiceImpl(priceRateServ);
  }

  @Test
  void exampleInput() {
    var ins = new Insurance();
    ins.setType(InsuranceType.VARIABLE);
    ins.setBundle(InsuranceBundle.EXTENDED);
    ins.setBonuses(Set.of(InsuranceBonus.SPORT));
    ins.setNumberOfPersons(2);
    ins.setStart(LocalDate.now());
    ins.setEnd(ins.getStart().plus(3, ChronoUnit.DAYS));

    Mockito.when(priceRateServ.bundlePrice(InsuranceBundle.EXTENDED, InsuranceType.VARIABLE))
        .thenReturn(new BigDecimal("1.8"));
    Mockito.when(priceRateServ.bonusPriceModifier(InsuranceBonus.SPORT, InsuranceType.VARIABLE))
    .thenReturn(new BigDecimal("0.3"));

    Assertions.assertEquals(new BigDecimal("14.04"), priceCalcServ.calculatePrice(ins));
  }
}
